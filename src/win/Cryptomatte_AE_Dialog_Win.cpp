
//
//	Cryptomatte AE plug-in
//		by Brendan Bolles <brendan@fnordware.com>
//
//
//	Part of ProEXR
//		http://www.fnordware.com/ProEXR
//
//


#include "Cryptomatte_AE_Dialog.h"

#include "Cryptomatte_AE_Wildcards.h"

#include "OpenEXR_UTF.h"

#include <windows.h>
//#include <Shlwapi.h>
//#include <ShlObj.h>

//#include <sstream>

#include <assert.h>



// dialog comtrols
enum {
	DLOG_noUI = -1,
	DLOG_OK = IDOK,
	DLOG_Cancel = IDCANCEL,
	DLOG_Layer_Menu = 3,
	DLOG_Selection_Text,
	DLOG_Manifest_Text,
	DLOG_Wildcard_Button
};


static HINSTANCE hDllInstance = NULL;

static WORD	g_item_clicked = 0;

static std::string g_layer;
static std::string g_selection;
static std::string g_manifest;
static const std::set<std::string> *g_layers = NULL;

static std::string g_current_layer;
static bool g_warned = false;


static void SetFieldFromString(HWND hDlg, int nIDDlgItem, const std::string &str)
{
	const size_t len = str.size() + 1;

	WCHAR *wstr = (WCHAR *)malloc(len * sizeof(WCHAR));

	if(wstr != NULL)
	{
		UTF8toUTF16(str, (utf16_char *)wstr, len);

		SetDlgItemText(hDlg, nIDDlgItem, wstr);

		free(wstr);
	}
}


static void SetStrFromField(HWND hDlg, int nIDDlgItem, std::string &str)
{
	const LRESULT len = SendMessage(GetDlgItem(hDlg, nIDDlgItem), WM_GETTEXTLENGTH, (WPARAM)0, (LPARAM)0) + 1;

	WCHAR *buf = (WCHAR *)malloc(len * sizeof(WCHAR));

	if(buf)
	{
		const LRESULT copied = SendMessage(GetDlgItem(hDlg, nIDDlgItem), WM_GETTEXT, (WPARAM)len, (LPARAM)buf);

		assert(copied == len - 1);

		str = UTF16toUTF8((utf16_char *)buf);

		free(buf);
	}
}


static void SetStrFromMenu(HWND hDlg, int nIDDlgItem, std::string &str)
{
	HWND layer_menu = GetDlgItem(hDlg, nIDDlgItem);

	const LRESULT selected = SendMessage(layer_menu, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);

	if(selected != CB_ERR)
	{
		const LRESULT len = SendMessage(layer_menu, (UINT)CB_GETLBTEXTLEN, (WPARAM)selected, (LPARAM)0) + 1;

		WCHAR *buf = (WCHAR *)malloc(len * sizeof(WCHAR));

		if(buf)
		{
			const LRESULT copied = SendMessage(layer_menu, (UINT)CB_GETLBTEXT, (WPARAM)selected, (LPARAM)buf);

			assert(copied == len - 1);

			str = UTF16toUTF8((utf16_char *)buf);

			free(buf);
		}
	}
	else
		str = "";
}


static BOOL CALLBACK DialogProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{ 
    BOOL fError; 

    switch(message) 
    { 
		case WM_INITDIALOG:
			do{
				HWND layer_menu = GetDlgItem(hwndDlg, DLOG_Layer_Menu);
				
				for(std::set<std::string>::const_iterator i = g_layers->begin(); i != g_layers->end(); ++i)
				{
					const std::string &layer_name = *i;

					const size_t len = layer_name.size() + 1;

					WCHAR *wstr = (WCHAR *)malloc(len * sizeof(WCHAR));

					if(wstr != NULL)
					{
						UTF8toUTF16(layer_name, (utf16_char *)wstr, len);

						const LRESULT idx = SendMessage(layer_menu, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)(LPCTSTR)wstr);

						if(g_layer == layer_name)
							SendMessage(layer_menu, CB_SETCURSEL, (WPARAM)idx, (LPARAM)0);

						free(wstr);
					}
				}

				SetFieldFromString(hwndDlg, DLOG_Selection_Text, g_selection.c_str());
				SetFieldFromString(hwndDlg, DLOG_Manifest_Text, g_manifest.c_str());

				g_current_layer = g_layer;
				g_warned = false;

				return TRUE;
			}while(0);

        case WM_COMMAND: 
			const WORD item_clicked = LOWORD(wParam);

			if(item_clicked == DLOG_Layer_Menu)
			{
				if(HIWORD(wParam) == CBN_SELCHANGE)
				{
					if(!g_warned)
					{
						std::string layer;
						SetStrFromMenu(hwndDlg, DLOG_Layer_Menu, layer);

						if(g_current_layer != layer)
						{
							std::string selection, manifest;
							SetStrFromField(hwndDlg, DLOG_Selection_Text, selection);
							SetStrFromField(hwndDlg, DLOG_Manifest_Text, manifest);

							if((!selection.empty()) || (!manifest.empty()))
							{
								MessageBox(hwndDlg, TEXT("Changing layers will probably invalidate the current selection and manifest."), TEXT("Cryptomatte"), MB_OK);

								g_warned = true;
							}

							g_current_layer = layer;
						}
					}

					return TRUE;
				}
			}
			else if(item_clicked == DLOG_OK || item_clicked == DLOG_Cancel || item_clicked == DLOG_Wildcard_Button)
			{
				g_item_clicked = item_clicked;

				switch(item_clicked) 
				{
					case DLOG_Wildcard_Button:
						do{
							std::string selection, manifest;
							SetStrFromField(hwndDlg, DLOG_Selection_Text, selection);
							SetStrFromField(hwndDlg, DLOG_Manifest_Text, manifest);

							if((!selection.empty()) && (!manifest.empty()))
							{
								if( ExpandWildcards(selection, manifest) )
								{
									SetFieldFromString(hwndDlg, DLOG_Selection_Text, selection.c_str());
								}
							}
							else
							{
								MessageBox(hwndDlg, TEXT("Wildcard expansion requires selection text with wildcards (* or ?) and a JSON manifest."), TEXT("Cryptomatte"), MB_OK);
							}
						}while(0);

						return TRUE;

					case DLOG_OK:
						do{
							SetStrFromMenu(hwndDlg, DLOG_Layer_Menu, g_layer);

							SetStrFromField(hwndDlg, DLOG_Selection_Text, g_selection);
							SetStrFromField(hwndDlg, DLOG_Manifest_Text, g_manifest);
						}while(0);

					case DLOG_Cancel:
						EndDialog(hwndDlg, 0);

						return TRUE;
				} 
			}
    } 

    return FALSE; 
}

bool Cryptomatte_Dialog(
	std::string					&layer,
	std::string					&selection,
	std::string					&manifest,
	const std::set<std::string>	&layers,
	const char					*plugHndl,
	const void					*mwnd)
{
	bool hit_ok = false;
	
	// set globals
	g_layer = layer;
	g_selection = selection;
	g_manifest = manifest;
	g_layers = &layers;


	int status = DialogBox((HINSTANCE)hDllInstance, TEXT("DIALOG"), (HWND)mwnd, (DLGPROC)DialogProc);


	if(g_item_clicked == DLOG_OK)
	{
		layer = g_layer;
		selection = g_selection;
		manifest = g_manifest;

		hit_ok = true;
	}

	return hit_ok;
}

BOOL WINAPI DllMain(HANDLE hInstance, DWORD fdwReason, LPVOID lpReserved)
{
	if (fdwReason == DLL_PROCESS_ATTACH)
		hDllInstance = (HINSTANCE)hInstance;

	return TRUE;   // Indicate that the DLL was initialized successfully.
}
