//
//	Cryptomatte AE plug-in
//		by Brendan Bolles <brendan@fnordware.com>
//
//
//	Part of ProEXR
//		http://www.fnordware.com/ProEXR
//
//

#import "Cryptomatte_AE_Wildcards_Obj.h"

#include "Cryptomatte_AE_Wildcards.h"

@implementation Cryptomatte_AE_Wildcards_Obj

+ (NSString *)expand:(NSString *)selection withManifest:(NSString *)manifest
{
	std::string selection_str = [selection UTF8String];
	const std::string manifest_str = [manifest UTF8String];

	if( ExpandWildcards(selection_str, manifest_str) )
	{
		return [NSString stringWithUTF8String:selection_str.c_str()];
	}
	else
		return nil;
}

@end
