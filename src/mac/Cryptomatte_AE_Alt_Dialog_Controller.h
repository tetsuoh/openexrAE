//
//	Cryptomatte AE plug-in
//		by Brendan Bolles <brendan@fnordware.com>
//
//
//	Part of ProEXR
//		http://www.fnordware.com/ProEXR
//
//

#import <Cocoa/Cocoa.h>


@interface KeyValue : NSObject
{

}

@property (retain) NSString *key;
@property (retain) id value;

- (id)init:(NSString *)key withValue:(id)value;
+ (id)key:(NSString *)key withValue:(id)value;

@end


@interface Cryptomatte_AE_Alt_Dialog_Controller : NSWindowController <NSTableViewDataSource, NSOutlineViewDataSource>
{
	
}

@property (assign) IBOutlet NSTableView *list;
@property (assign) IBOutlet NSOutlineView *tree;
@property (assign) IBOutlet NSButton *addButton;
@property (assign) IBOutlet NSButton *removeButton;
@property (assign) IBOutlet NSTextField *selectionInfo;

@property (retain) NSArray *objectArray;
@property (retain) KeyValue *objectTree;


- (id)init:(NSArray *)selection
	manifest:(NSArray *)manifest;

- (IBAction)clickOK:(NSButton *)sender;
- (IBAction)clickCancel:(NSButton *)sender;

- (IBAction)clickTreeAdd:(NSButton *)sender;
- (IBAction)clickTreeRemove:(NSButton *)sender;

- (NSArray *)getSelection;

@end

