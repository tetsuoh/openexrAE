//
//	EXtractoR
//		by Brendan Bolles <brendan@fnordware.com>
//
//	extract float channels
//
//	Part of the fnord OpenEXR tools
//		http://www.fnordware.com/OpenEXR/
//
//
//	mmm...our custom UI
//
//

#include "EXtractoR.h"

#include "EXtractoR_Dialog.h"

#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
#include "OpenEXR_UTF.h"
#else
#include "DrawbotBot.h"
#endif


#if PF_AE_PLUG_IN_VERSION >= PF_AE100_PLUG_IN_VERSION
#include <string>
#include <list>
#endif


using namespace std;


#if PF_AE_PLUG_IN_VERSION >= PF_AE100_PLUG_IN_VERSION

#define TEXT_COLOR      PF_App_Color_TEXT_DISABLED

#define kMENU_TEXT_INDENT_H  10
#define kMENU_TEXT_INDENT_V  2

#define kMENU_ARROW_WIDTH    14
#define kMENU_ARROW_HEIGHT   7

#define kMENU_ARROW_SPACE_H  8
#define kMENU_ARROW_SPACE_V  7

#define kMENU_SHADOW_OFFSET  3

static void DrawMenu(DrawbotBot &bot, const char *label, const char *item, unsigned int menu_width, bool layer_menu)
{
    DRAWBOT_PointF32 original = bot.Pos();
	
    float text_height = bot.FontSize();
	
    bot.Move(kMENU_LABEL_WIDTH, kMENU_TEXT_INDENT_V + text_height);
	
    bot.SetColor(TEXT_COLOR);
    bot.DrawString(label, kDRAWBOT_TextAlignment_Right);
	
    bot.Move(kMENU_LABEL_SPACE, -(kMENU_TEXT_INDENT_V + text_height));
	
    DRAWBOT_PointF32 menu_corner = bot.Pos();
	
    bot.Move(kMENU_SHADOW_OFFSET, kMENU_SHADOW_OFFSET);
    bot.SetColor(PF_App_Color_BLACK, 0.3f);
    bot.PaintRect(menu_width, kMENU_HEIGHT);
    bot.MoveTo(menu_corner);
	
    bot.SetColor(layer_menu ? PF_App_Color_SHADOW_DISABLED : PF_App_Color_SHADOW);
    bot.PaintRect(menu_width, kMENU_HEIGHT);
	
    bot.SetColor(PF_App_Color_HILITE);
    bot.DrawRect(menu_width, kMENU_HEIGHT);
	
    bot.Move(kMENU_TEXT_INDENT_H, kMENU_TEXT_INDENT_V + text_height);
	
    bot.SetColor(TEXT_COLOR);
    bot.DrawString(item,
                    kDRAWBOT_TextAlignment_Left,
                    kDRAWBOT_TextTruncation_EndEllipsis,
                    menu_width - kMENU_TEXT_INDENT_H - kMENU_TEXT_INDENT_H -
                        kMENU_ARROW_WIDTH - kMENU_ARROW_SPACE_H - kMENU_ARROW_SPACE_H);
	
    bot.MoveTo(menu_corner.x + menu_width - kMENU_ARROW_SPACE_H - kMENU_ARROW_WIDTH,
                menu_corner.y + kMENU_ARROW_SPACE_V);
	
    bot.SetColor(PF_App_Color_LIGHT_TINGE);
    bot.PaintTriangle(kMENU_ARROW_WIDTH, kMENU_ARROW_HEIGHT);
	
    bot.MoveTo(original);
}


// layer.R -> R
static string
channelName(const string &full_channel_name)
{
	size_t pos = full_channel_name.rfind('.');
	
	if(pos != string::npos && pos != 0 && pos + 1 < full_channel_name.size())
	{
		return full_channel_name.substr(pos + 1);
	}
	else
		return full_channel_name;
}


static bool
channelOrderCompare(const string &first, const string &second)
{
	static const char * const channel_list[] = {"R", "r", "red",
												"G", "g", "green",
												"B", "b", "blue",
												"A", "a", "alpha"};
	
	int first_rank = 12,
		second_rank = 12;
	
	string first_channel = channelName(first);
	string second_channel = channelName(second);
	
	for(int i=0; i < 12; i++)
	{
		if(first_channel == channel_list[i])
			first_rank = i;
		
		if(second_channel == channel_list[i])
			second_rank = i;
	}
	
	if(first_rank == second_rank)
	{
		return (strcmp(first.c_str(), second.c_str()) <= 0);
	}
	else
	{
		return (first_rank < second_rank);
	}
}


static PF_Err
DoClick(
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_EventExtra	*event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	event_extra->evt_out_flags = PF_EO_NONE;
	
	if(event_extra->effect_win.area == PF_EA_CONTROL)
	{
        PF_Point local_point;
		
        local_point.h = event_extra->u.do_click.screen_point.h - event_extra->effect_win.current_frame.left;
        local_point.v = event_extra->u.do_click.screen_point.v - event_extra->effect_win.current_frame.top;
		
		const PF_Rect &control_rect = event_extra->effect_win.current_frame;
		
		const unsigned int menu_width = MAX(kMENU_WIDTH, control_rect.right - RIGHT_PLACE(control_rect.left) - kMENU_LABEL_WIDTH - kMENU_LABEL_SPACE - kMENU_SHADOW_OFFSET);

        if(local_point.h > RIGHT_PLACE(control_rect.left) && local_point.h < (RIGHT_PLACE(control_rect.left) + menu_width))
        {
			if(local_point.v > kUI_CONTROL_DOWN && local_point.v < (kUI_CONTROL_DOWN + (INFO_TOTAL_ITEMS * kUI_CONTROL_STEP) - (kUI_CONTROL_STEP - kMENU_HEIGHT)))
			{
				const int which_menu = (local_point.v - kUI_CONTROL_DOWN) / kUI_CONTROL_STEP;
				
				if((local_point.v - kUI_CONTROL_DOWN - (which_menu * kUI_CONTROL_STEP)) < kMENU_HEIGHT)
				{
					AEGP_SuiteHandler suites(in_data->pica_basicP);
					PF_ChannelSuite *cs = suites.PFChannelSuite();

					A_long chan_count = 0;
					cs->PF_GetLayerChannelCount(in_data->effect_ref, 0, &chan_count);

					if(chan_count == 0 || err)
					{
						PF_SPRINTF(out_data->return_msg, "No auxiliary channels available.");
					}
					else
					{
						ArbitraryData *arb_data = (ArbitraryData *)PF_LOCK_HANDLE(params[EXTRACT_DATA]->u.arb_d.value);
						ExtractStatus *extract_status = (ExtractStatus *)PF_LOCK_HANDLE(in_data->sequence_data);
						
						// reconcile incorrect indices
						if(extract_status->red.status == STATUS_INDEX_CHANGE) { arb_data->red.index = extract_status->red.index; }
						if(extract_status->green.status == STATUS_INDEX_CHANGE) { arb_data->green.index = extract_status->green.index; }
						if(extract_status->blue.status == STATUS_INDEX_CHANGE) { arb_data->blue.index = extract_status->blue.index; }
						if(extract_status->alpha.status == STATUS_INDEX_CHANGE) { arb_data->alpha.index = extract_status->alpha.index; }
						
					#ifdef MAC_ENV
						const void *hwnd = NULL;
					#else
						HWND *hwnd = NULL;
						PF_GET_PLATFORM_DATA(PF_PlatData_MAIN_WND, &hwnd);
					#endif
						
						ChannelData	*channel_data[4] = {&arb_data->red,
														&arb_data->green,
														&arb_data->blue,
														&arb_data->alpha };
						
						ChannelStatus *channel_status[4] = {&extract_status->red,
															&extract_status->green,
															&extract_status->blue,
															&extract_status->alpha};

						Imf::ChannelList channels;
						map<string, int> index_map;
						
						for(int i=0; i < chan_count; i++)
						{
							PF_Boolean	found;

							PF_ChannelRef chan_ref;
							PF_ChannelDesc chan_desc;
							
							// get the channel
							cs->PF_GetLayerChannelIndexedRefAndDesc(in_data->effect_ref,
																	0,
																	i,
																	&found,
																	&chan_ref,
																	&chan_desc);
							
							// found isn't really what it used to be
							found = (found && chan_desc.channel_type && chan_desc.data_type);
							
							if(found && chan_desc.dimension == 1 && chan_desc.data_type == PF_DataType_FLOAT)
							{
								channels.insert(chan_desc.name, Imf::Channel(Imf::FLOAT));
								index_map[chan_desc.name] = i;
							}
						}
						
						
						if(index_map.size() == 0)
						{
							PF_SPRINTF(out_data->return_msg, "No float auxiliary channels available.");
						}
						else
						{
							if(which_menu == INFO_LAYERS)
							{
								// use EXR code to merge channels into layers
								set<string> layerNames;
								channels.layers(layerNames);
								
								LayerMap layers;
								
								for(set<string>::const_iterator i = layerNames.begin(); i != layerNames.end(); i++)
								{
									// put into a list so we can use the sort function
									Imf::ChannelList::ConstIterator f, l;
									channels.channelsInLayer(*i, f, l);
									
									list<string> channels_in_layer;
									
									for(Imf::ChannelList::ConstIterator j = f; j != l; j++)
										channels_in_layer.push_back(j.name());
									
									if(channels_in_layer.size() > 1)
									{
										// sort into RGBA
										channels_in_layer.sort(channelOrderCompare);
									}
									
									// copy our list into a vector
									ChannelVec chans;
									
									for(list<string>::const_iterator j = channels_in_layer.begin(); j != channels_in_layer.end() && chans.size() < 4; ++j)
									{
										chans.push_back(*j);
									}
									
									layers[*i] = chans;
								}
								
								
								// make layers for channels that aren't part of a layer situation, like "Z"
								for(Imf::ChannelList::ConstIterator chan = channels.begin(); chan != channels.end(); ++chan)
								{
									const string &name = chan.name();
									
									if(name.find('.') == string::npos &&
										name != "R" && name != "G" && name != "B" && name != "A")
									{
										ChannelVec chans;
										
										chans.push_back(name);
										
										layers[name] = chans;
									}
								}
								
								
								if(layers.size() > 0)
								{
									MenuVec menuVec;
									
									for(LayerMap::const_iterator i = layers.begin(); i != layers.end(); ++i)
									{
										const string &layerName = i->first;
										
										menuVec.push_back(layerName);
									}
									
									
									const int layerIndex = PopUpMenu(menuVec, -1, hwnd);
									
									if(layerIndex >= 0)
									{
										const ChannelVec &chans = layers[menuVec[layerIndex]];
										
										if(chans.size() == 1)
										{
											for(int i=0; i < 3; i++)
											{
												channel_data[i]->action = DO_EXTRACT;
												channel_data[i]->index = index_map[chans[0]];
												strncpy(channel_data[i]->name, chans[0].c_str(), MAX_CHANNEL_NAME_LEN);
												channel_status[i]->status = STATUS_NORMAL;
											}
										
											channel_data[3]->action = DO_COPY;
											channel_status[3]->status = STATUS_NORMAL;
										}
										else
										{
											for(int i=0; i < 4; i++)
											{
												if(i < chans.size())
												{
													channel_data[i]->action = DO_EXTRACT;
													channel_data[i]->index = index_map[chans[i]];
													strncpy(channel_data[i]->name, chans[i].c_str(), MAX_CHANNEL_NAME_LEN);
													channel_status[i]->status = STATUS_NORMAL;
												}
												else
												{
													channel_data[i]->action = DO_COPY;
													channel_status[i]->status = STATUS_NORMAL;
												}
											}
										}
										
										params[EXTRACT_DATA]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE;
									}
								}
							}
							else
							{
								const int chan_num = (which_menu - 1);
								
								MenuVec menuVec;
								int selected_idx = -1;
								
								menuVec.push_back("(copy)");
								
								if(channel_data[chan_num]->action == DO_COPY)
									selected_idx = 0;
								
								for(Imf::ChannelList::ConstIterator chan = channels.begin(); chan != channels.end(); ++chan)
								{
									const string &name = chan.name();
									
									menuVec.push_back(name);
									
									if(name == channel_data[chan_num]->name && channel_data[chan_num]->action == DO_EXTRACT)
										selected_idx = (menuVec.size() - 1);
								}
								
								
								const int popUpChoice = PopUpMenu(menuVec, selected_idx, hwnd);
								
								if(popUpChoice == 0)
								{
									channel_data[chan_num]->action = DO_COPY;
									channel_status[chan_num]->status = STATUS_NORMAL;
									
									params[EXTRACT_DATA]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE;
								}
								else if(popUpChoice > 0)
								{
									channel_data[chan_num]->action = DO_EXTRACT;
									channel_data[chan_num]->index = index_map[menuVec[popUpChoice]];
									strncpy(channel_data[chan_num]->name, menuVec[popUpChoice].c_str(), MAX_CHANNEL_NAME_LEN);
									channel_status[chan_num]->status = STATUS_NORMAL;
									
									params[EXTRACT_DATA]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE;
								}
							}
						}
						
						event_extra->evt_out_flags = PF_EO_HANDLED_EVENT;
						
						PF_UNLOCK_HANDLE(params[EXTRACT_DATA]->u.arb_d.value);
						PF_UNLOCK_HANDLE(in_data->sequence_data);
					}
				}
			}
        }
	}
	
	return err;
}

#endif // PF_AE_PLUG_IN_VERSION >= PF_AE100_PLUG_IN_VERSION


static PF_Err
DrawEvent(	
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_EventExtra	*event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	event_extra->evt_out_flags = PF_EO_NONE;
	
	if(!(event_extra->evt_in_flags & PF_EI_DONT_DRAW) && params[EXTRACT_DATA]->u.arb_d.value != NULL)
	{
		if(event_extra->effect_win.area == PF_EA_CONTROL)
		{
			ArbitraryData *arb_data = (ArbitraryData *)PF_LOCK_HANDLE(params[EXTRACT_DATA]->u.arb_d.value);
			ExtractStatus *extract_status = (ExtractStatus *)PF_LOCK_HANDLE(in_data->sequence_data);
		
		#define MAX_MESSAGE_LEN 127
			A_char	red_message[MAX_MESSAGE_LEN],
					green_message[MAX_MESSAGE_LEN],
					blue_message[MAX_MESSAGE_LEN],
					alpha_message[MAX_MESSAGE_LEN];
					
			A_char	*channel_message[4] = {	red_message,
											green_message,
											blue_message,
											alpha_message };
			
			ChannelStatus *channel_status[4] = {	&extract_status->red,
													&extract_status->green,
													&extract_status->blue,
													&extract_status->alpha };
												
			ChannelData *channel_data[4] = {	&arb_data->red,
												&arb_data->green,
												&arb_data->blue,
												&arb_data->alpha };
		
			// set up the messages
			for(int i=0; i < 4; i++)
			{
				if(channel_data[i]->action == DO_COPY)
				{
					strcpy(channel_message[i], "(copy)");
				}
				else if(channel_data[i]->action == DO_EXTRACT)
				{
					// start with the name
					strcpy(channel_message[i], channel_data[i]->name);
					
					// process status returned from Render()
					if(channel_status[i]->status == STATUS_NOT_FOUND)
					{
						strcat(channel_message[i], " (not found)");
					}
					else if(channel_status[i]->status == STATUS_NO_CHANNELS)
					{
						strcat(channel_message[i], " (none available)");
					}
					else if(channel_status[i]->status == STATUS_ERROR)
					{
						strcat(channel_message[i], " (error)");
					}
					// can't change params during draw, need a click
					//else if(channel_status[i]->status == STATUS_INDEX_CHANGE)
					//{
					//	channel_data[i]->index = channel_status[i]->index;
					//	params[EXTRACT_DATA]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE;
					//}
				}
			}
			
			const PF_Rect &control_rect = event_extra->effect_win.current_frame;
			

#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION

		// old-school Carbon/Win32 drawing code
		
		AEGP_SuiteHandler suites(in_data->pica_basicP);

		#ifdef WIN_ENV
			void*			dp = (*(event_extra->contextH))->cgrafptr;
			HDC				ecw_hdc	=	0;
			PF_GET_CGRAF_DATA(dp, PF_CGrafData_HDC, (void **)&ecw_hdc);
			PF_Point		pen;
		#endif

			PF_App_Color	pf_bg_color;
			suites.AppSuite()->PF_AppGetBgColor(&pf_bg_color);
			
		#ifdef MAC_ENV
			RGBColor titleColor, itemColor;
			RGBColor		bg_color;
	
			bg_color.red	=	pf_bg_color.red;
			bg_color.green	=	pf_bg_color.green;
			bg_color.blue	=	pf_bg_color.blue;
			
			RGBBackColor(&bg_color);
		
			EraseRect(&control_rect);
			
			GetForeColor(&itemColor);
			
			titleColor.red   = TITLE_COMP(itemColor.red);
			titleColor.green = TITLE_COMP(itemColor.green);
			titleColor.blue  = TITLE_COMP(itemColor.blue);
				
			RGBForeColor(&titleColor);
		#endif

		#ifdef MAC_ENV
		#define DRAW_STRING(STRING)	DrawText(STRING, 0, strlen(STRING));
		#define MOVE_TO(H, V)		MoveTo( (H), (V) );
		#else
		#define DRAW_STRING(STRING) \
			do{ \
				WCHAR u_string[100]; \
				UTF8toUTF16(STRING, (utf16_char *)u_string, 99); \
				TextOut(ecw_hdc, pen.h, pen.v, u_string, strlen(STRING)); \
			}while(0);
		#define MOVE_TO(H, V)		MoveToEx(ecw_hdc, (H), (V), NULL); pen.h = (H); pen.v = (V);
		#endif

			// Property Headings
			MOVE_TO(RIGHT_STATUS(control_rect.left), DOWN_PLACE(INFO_RED, control_rect.top) );
			DRAW_STRING("Red");
			
			MOVE_TO(RIGHT_STATUS(control_rect.left), DOWN_PLACE(INFO_GREEN, control_rect.top) );
			DRAW_STRING("Green");

			MOVE_TO(RIGHT_STATUS(control_rect.left), DOWN_PLACE(INFO_BLUE, control_rect.top) );
			DRAW_STRING("Blue");

			MOVE_TO(RIGHT_STATUS(control_rect.left), DOWN_PLACE(INFO_ALPHA, control_rect.top) );
			DRAW_STRING("Alpha");
			
			//Property Items
			PF_Point prop_origin;
			prop_origin.v = control_rect.top;
			
	#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
		#ifdef MAC_ENV
			RGBForeColor(&itemColor);
			prop_origin.h = control_rect.left + TextWidth("Alpha", 0, strlen("Alpha"));
		#else
			SIZE text_dim = {15, 8};
			GetTextExtentPoint(ecw_hdc, TEXT("Alpha"), strlen("Alpha"), &text_dim);
			prop_origin.h = control_rect.left + text_dim.cx;
		#endif
	#else
			current_brush = &item_brushP;
			prop_origin.h = control_rect.left + 50;
	#endif
			
			MOVE_TO(RIGHT_PROP(prop_origin.h), DOWN_PLACE(INFO_RED, prop_origin.v) );
			DRAW_STRING(red_message);
			
			MOVE_TO(RIGHT_PROP(prop_origin.h), DOWN_PLACE(INFO_GREEN, prop_origin.v) );
			DRAW_STRING(green_message);

			MOVE_TO(RIGHT_PROP(prop_origin.h), DOWN_PLACE(INFO_BLUE, prop_origin.v) );
			DRAW_STRING(blue_message);

			MOVE_TO(RIGHT_PROP(prop_origin.h), DOWN_PLACE(INFO_ALPHA, prop_origin.v) );
			DRAW_STRING(alpha_message);


#else // PF_AE_PLUG_IN_VERSION >= PF_AE100_PLUG_IN_VERSION

			// new-school Drawbot drawing code
			
			DrawbotBot bot(in_data->pica_basicP, event_extra->contextH, in_data->appl_id);
			
			const unsigned int menu_width = MAX(kMENU_WIDTH, control_rect.right - RIGHT_PLACE(control_rect.left) - kMENU_LABEL_WIDTH - kMENU_LABEL_SPACE - kMENU_SHADOW_OFFSET);
			
			bot.MoveTo(RIGHT_PLACE(control_rect.left), DOWN_PLACE(INFO_LAYERS, control_rect.top));
			DrawMenu(bot, "Layers:", "", menu_width, true);
			
			bot.MoveTo(RIGHT_PLACE(control_rect.left), DOWN_PLACE(INFO_RED, control_rect.top));
			DrawMenu(bot, "Red:", red_message, menu_width, false);
			
			bot.MoveTo(RIGHT_PLACE(control_rect.left), DOWN_PLACE(INFO_GREEN, control_rect.top));
			DrawMenu(bot, "Green:", green_message, menu_width, false);
			
			bot.MoveTo(RIGHT_PLACE(control_rect.left), DOWN_PLACE(INFO_BLUE, control_rect.top));
			DrawMenu(bot, "Blue:", blue_message, menu_width, false);

			bot.MoveTo(RIGHT_PLACE(control_rect.left), DOWN_PLACE(INFO_ALPHA, control_rect.top));
			DrawMenu(bot, "Alpha:", alpha_message, menu_width, false);

#endif //PF_AE_PLUG_IN_VERSION >= PF_AE100_PLUG_IN_VERSION
			
			event_extra->evt_out_flags = PF_EO_HANDLED_EVENT;

			PF_UNLOCK_HANDLE(params[EXTRACT_DATA]->u.arb_d.value);
			PF_UNLOCK_HANDLE(in_data->sequence_data);
		}
	}

	return err;
}


PF_Err
HandleEvent ( 
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_EventExtra	*extra )
{
	PF_Err err = PF_Err_NONE;
	
	if(!err)
	{
		switch(extra->e_type)
		{
			case PF_Event_DRAW:
				err = DrawEvent(in_data, out_data, params, output, extra);
				break;
			
			case PF_Event_DO_CLICK:
			#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
				err = DoDialog(in_data, out_data, params, output);
				extra->evt_out_flags = PF_EO_HANDLED_EVENT;
			#else
				err = DoClick(in_data, out_data, params, output, extra);
			#endif
				break;
				
			case PF_Event_ADJUST_CURSOR:
			#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
			#if defined(MAC_ENV)
				#if PF_AE_PLUG_IN_VERSION >= PF_AE100_PLUG_IN_VERSION
				SetMickeyCursor(); // the cute mickey mouse hand
				#else
				SetThemeCursor(kThemePointingHandCursor);
				#endif
				extra->u.adjust_cursor.set_cursor = PF_Cursor_CUSTOM;
			#else
				extra->u.adjust_cursor.set_cursor = PF_Cursor_FINGER_POINTER;
			#endif
				extra->evt_out_flags = PF_EO_HANDLED_EVENT;
			#endif // PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
				break;
		}
	}
	
	return err;
}
