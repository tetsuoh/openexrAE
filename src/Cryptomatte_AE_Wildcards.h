//
//	Cryptomatte AE plug-in
//		by Brendan Bolles <brendan@fnordware.com>
//
//
//	Part of ProEXR
//		http://www.fnordware.com/ProEXR
//
//

#pragma once

#ifndef _CRYPTOMATTE_WILDCARDS_H_
#define _CRYPTOMATTE_WILDCARDS_H_

#include <string>

bool ExpandWildcards(std::string &selection, const std::string &manifest);

#endif // _CRYPTOMATTE_WILDCARDS_H_
